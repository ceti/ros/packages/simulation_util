A ROS package for Gazebo simulation utility libraries.

The full documentation for this package is located [here](http://st.inf.tu-dresden.de/ceti-robots/pkgs/simulation_util.html).

## Provided Libraries

#### gazebo_zone_utility
- A library for displaying objects in Gazebo. Currently, only boxes are supported.
  - `GazeboZoneSpawner::spawnPrimitive()` spawns a primitive with visual, collision, and (opionally) physical
    properties.
  - `GazeboZoneSpawner::spawnVisualPrimitive()` also spawns objects, but these are purely visual, such that any
    physical object moves straight through them. This is useful for additional visualizations, e.g., to describe
    certain zones in the scene and to display paraphysical entities, such as ghosts.